<?php

namespace modules\order\commands\category;

use modules\order\services\CategoryTreeSynchronizator;
use yii\base\Action;
use yii\db\Exception;

/**
 * Class FillAction
 *
 * @package modules\order\commands\category
 */
class FillAction extends Action
{
    /**
     * @var CategoryTreeSynchronizator
     */
    private $sync;

    /**
     * FillAction constructor.
     *
     * @param $id
     * @param $controller
     * @param CategoryTreeSynchronizator $sync
     * @param array $config
     */
    public function __construct($id, $controller, CategoryTreeSynchronizator $sync, $config = [])
    {
        parent::__construct($id, $controller, $config);
        $this->sync = $sync;
    }

    /**
     * Зарллгение категорий из внешнего сервиса
     *
     * @throws Exception
     */
    public function run()
    {
        $this->sync->sync();
    }
}
