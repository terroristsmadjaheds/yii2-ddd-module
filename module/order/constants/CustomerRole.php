<?php

namespace modules\order\constants;

use modules\core\common\constants\Base;

/**
 * Class CustomerRole
 *
 * @package modules\order\constants
 */
class CustomerRole extends Base
{
    public const PRIVILEGED = "_privileged";
    public const GENERAL = "_general";
    public const VIP_DELIVERY = "vip_delivery";

    /**
     * @inheritDoc
     */
    public static function getLabels()
    {
        return [
            static::PRIVILEGED => "Привилегированный покупатель",
            static::GENERAL => "Обычный покупатель",
            static::VIP_DELIVERY => "VIP-доставка"
        ];
    }
}
