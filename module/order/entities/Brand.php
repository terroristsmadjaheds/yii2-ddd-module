<?php

namespace modules\order\entities;

/**
 * Class Brand
 *
 * @package modules\order\entities
 */
class Brand
{
    /**
     * @var string
     */
    private $id = "";

    /**
     * @var string
     */
    private $name = "";

    /**
     * @var string
     */
    private $slug = "";

    /**
     * Brand constructor.
     *
     * @param string $id
     * @param string $name
     * @param string $slug
     */
    public function __construct(string $id, string $name, string $slug)
    {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = json_encode([
            "id" => $this->getId(),
            "name" => $this->getName(),
            "slug" => $this->getSlug()
        ]);

        return $result !== false
            ? $result
            : "";
    }
}
