<?php

namespace modules\order\entities;

use modules\order\constants\CustomerRole;
use modules\order\exceptions\InvalidCustomerRoleException;

/**
 * Class Customer
 *
 * @package modules\order\entities
 */
class Customer
{
    /**
     * @var LoyaltyCard
     */
    private $loyaltyCard;

    /**
     * @var float
     */
    private $totalSpentAmount = 0.0;

    /**
     * @var string[] список ролей покупателя
     * Ключ массива - роль
     * @see CustomerRole
     */
    private $roles = [];

    /**
     * Customer constructor.
     * @param LoyaltyCard $loyaltyCard
     */
    public function __construct(LoyaltyCard $loyaltyCard)
    {
        $this->loyaltyCard = $loyaltyCard;
    }

    /**
     * @return bool
     */
    public function isPrivileged(): bool
    {
        return $this->hasRole(CustomerRole::PRIVILEGED);
    }

    /**
     * @return bool
     */
    public function isGeneral(): bool
    {
        return $this->hasRole(CustomerRole::GENERAL);
    }

    /**
     * @return LoyaltyCard
     */
    public function getLoyaltyCard(): LoyaltyCard
    {
        return $this->loyaltyCard;
    }

    /**
     * @return float
     */
    public function getTotalSpentAmount(): float
    {
        return $this->totalSpentAmount;
    }

    /**
     * @param float $totalSpentAmount
     *
     * @return Customer
     */
    public function setTotalSpentAmount(float $totalSpentAmount): self
    {
        $this->totalSpentAmount = $totalSpentAmount;

        return $this;
    }

    /**
     * Установка ролей покупателя
     *
     * @param string[] $roles набор ролей
     * @see CustomerRole
     *
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        try {
            CustomerRole::existsAll($roles);
        } catch (\InvalidArgumentException $e) {
            throw new InvalidCustomerRoleException(
                $e->getMessage(),
                $e->getCode(),
                $e
            );
        }

        $this->roles = array_flip($roles);

        return $this;
    }

    /**
     * Установка только известных нам ролей покупателя
     *
     * @param string[] $roles набор ролей
     * @see CustomerRole
     * @return $this
     */
    public function setExistsRoles(array $roles): self
    {
        $roles = array_filter($roles, function ($role) {
            return CustomerRole::check($role);
        });

        return $this->setRoles($roles);
    }

    /**
     * Получение списка ролей покупателя
     *
     * @return string[]
     * @see CustomerRole
     */
    public function getRoles(): array
    {
        return array_keys($this->roles);
    }

    /**
     * Проверка на наличие роли у покупателя
     *
     * @param string $role какую роль проверяем
     *
     * @return bool
     */
    private function hasRole(string $role): bool
    {
        return array_key_exists($role, $this->roles);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $data = [
            "loyalty_card" => [
                "color" => $this->getLoyaltyCard()->getColor(),
            ],
            "roles" => $this->getRoles(),
            "total_spent_amount" => $this->getTotalSpentAmount(),
        ];

        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return $json ? $json : "unable to encode";
    }
}
