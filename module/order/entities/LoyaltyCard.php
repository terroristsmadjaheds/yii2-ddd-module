<?php

namespace modules\order\entities;

/**
 * Class LoyaltyCard
 * @package modules\order\entities
 */
class LoyaltyCard
{
    /**
     * @var string
     */
    private $color = "";

    /**
     * LoyaltyCard constructor.
     * @param string $color
     */
    public function __construct(string $color)
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }
}
