<?php

namespace modules\order\entities;

use InvalidArgumentException;
use modules\currency\CurrencyOperation;
use modules\order\constants\OrderType;

/**
 * Class Order
 *
 * @package modules\order\entities
 */
class Order
{
    /**
     * @var string $id
     */
    private $id = 0;

    /**
     * @var Customer|null $customer
     */
    private $customer;

    /**
     * @var Delivery $delivery
     */
    private $delivery;

    /**
     * @var Position[] $positions
     */
    private $positions = [];

    /**
     * @var string
     */
    private $type = OrderType::UNDEFINED;

    /**
     * @var int
     */
    private $quantity = 0;

    /**
     * @var int
     */
    private $discountPercent = 0;

    /**
     * @var Tag[]
     */
    private $tags = [];

    /**
     * @var Category[]
     */
    private $categories = [];

    /**
     * @var Brand[]
     */
    private $brands = [];

    /**
     * @var Stock[]
     */
    private $commonStocks = [];

    /**
     * @var Feature[]
     */
    private $features = [];

    /**
     * @var string
     */
    private $number = "";

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->delivery = new Delivery();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Customer|null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer|null $customer
     *
     * @return $this
     */
    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Delivery
     */
    public function getDelivery(): Delivery
    {
        return $this->delivery;
    }

    /**
     * @param Delivery $delivery
     *
     * @return $this
     */
    public function setDelivery(Delivery $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * @return Position[]
     */
    public function getPositions(): array
    {
        return $this->positions;
    }

    /**
     * @param Position[] $positions
     *
     * @return Order
     */
    public function setPositions(array $positions): self
    {
        // сброс расчитываемых значений
        $this->quantity = 0;
        $this->discountPercent = 0;
        $this->tags = [];
        $this->categories = [];
        $this->brands = [];
        $this->commonStocks = [];

        $price = 0;
        $priceOriginal = 0;
        $tags = [];
        $categories = [];
        $brands = [];
        $stocks = [];
        $features = [];

        foreach ($positions as $position) {
            if (!$position instanceof Position) {
                throw new InvalidArgumentException("Элемент должен быть объектом класса " . Position::class);
            }

            $this->quantity += $position->getQuantity();
            $price += $position->getPrice();
            $priceOriginal += $position->getPriceOriginal();
            $tags = array_merge($tags, $position->getTags());

            $categories = array_merge($categories, $position->getCategories());
            $brands = array_merge($brands, $position->getBrands());

            $features = array_merge($features, $position->getFeatures());

            if (empty($stocks)) {
                $stocks = $position->getStocks();
            } else {
                $stocks = array_intersect($stocks, $position->getStocks());
            }
        }

        $this->positions = $positions;

        $this->tags = array_values(array_unique($tags, SORT_STRING));

        $this->categories = array_values(array_unique($categories, SORT_STRING));
        $this->brands = array_values(array_unique($brands, SORT_STRING));
        $this->features = array_values(array_unique($features, SORT_STRING));

        $this->commonStocks = array_filter(array_values($stocks), function ($s) {
            if ($s instanceof Stock) {
                return $s->getCount() > 0;
            }

            return true;
        });

        if (CurrencyOperation::gt($priceOriginal, 0)) {
            $this->discountPercent = CurrencyOperation::div(CurrencyOperation::sub($priceOriginal, $price), CurrencyOperation::div($priceOriginal, 100));
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Order
     */
    public function setType(string $type): self
    {
        if (!OrderType::check($type)) {
            throw new InvalidArgumentException(sprintf(
                "Передан неизвестный тип закза: %s. Возможные статусы: %s",
                $type,
                implode(", ", OrderType::getAll())
            ));
        }
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getDiscountPercent(): float
    {
        return $this->discountPercent;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return Category[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @return Brand[]
     */
    public function getBrands(): array
    {
        return $this->brands;
    }

    /**
     * @return Stock[]
     */
    public function getCommonStocks(): array
    {
        return $this->commonStocks;
    }

    /**
     * @return Feature[]
     */
    public function getFeatures(): array
    {
        return $this->features;
    }

    /**
     * @param string $number
     * @return Order
     */
    public function setNumber(string $number): Order
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }
}
