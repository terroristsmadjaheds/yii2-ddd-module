<?php

namespace modules\order\entities;

/**
 * Class Position
 *
 * @package modules\order\entities
 */
class Position
{
    /**
     * @var string $id
     */
    private $id = "";

    /**
     * @var int $quantity
     */
    private $quantity = 0;

    /**
     * @var Category[]
     */
    private $categories = [];

    /**
     * @var bool
     */
    private $areCategoriesDirty = false;

    /**
     * @var Brand[]
     */
    private $brands = [];

    /**
     * @var bool
     */
    private $areBrandsDirty = false;

    /**
     * @var Tag[]
     */
    private $tags = [];

    /**
     * @var bool
     */
    private $areTagsDirty = false;

    /**
     * @var float
     */
    private $priceOriginal = 0.0;

    /**
     * @var bool
     */
    private $isPriceOriginalDirty = false;

    /**
     * @var float
     */
    private $price = 0.0;

    /**
     * @var bool
     */
    private $isPriceDirty = false;

    /**
     * @var string
     */
    private $preorderText = "";

    /**
     * @var bool
     */
    private $isPreorderTextDirty = false;

    /**
     * @var Stock[]
     */
    private $stocks = [];

    /**
     * @var bool
     */
    private $areStocksDirty = false;

    /**
     * @var Feature[]
     */
    private $features = [];

    /**
     * @var bool
     */
    private $areFeaturesDirty = false;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return $this
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return Category[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param Category[] $categories
     *
     * @return Position
     */
    public function setCategories(array $categories): self
    {
        $this->categories = $categories;
        $this->areCategoriesDirty = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function areCategoriesDirty(): bool
    {
        return $this->areCategoriesDirty;
    }

    /**
     * @return Brand[]
     */
    public function getBrands(): array
    {
        return $this->brands;
    }

    /**
     * @param Brand[] $brands
     *
     * @return Position
     */
    public function setBrands(array $brands): self
    {
        $this->brands = $brands;
        $this->areBrandsDirty = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function areBrandsDirty(): bool
    {
        return $this->areBrandsDirty;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param Tag[] $tags
     *
     * @return Position
     */
    public function setTags(array $tags): self
    {
        $this->tags = $tags;
        $this->areTagsDirty = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function areTagsDirty(): bool
    {
        return $this->areTagsDirty;
    }

    /**
     * @return float
     */
    public function getPriceOriginal(): float
    {
        return $this->priceOriginal;
    }

    /**
     * @param float $priceOriginal
     *
     * @return Position
     */
    public function setPriceOriginal(float $priceOriginal): self
    {
        $this->priceOriginal = $priceOriginal;
        $this->isPriceOriginalDirty = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPriceOriginalDirty(): bool
    {
        return $this->isPriceOriginalDirty;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return Position
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;
        $this->isPriceDirty = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPriceDirty(): bool
    {
        return $this->isPriceDirty;
    }

    /**
     * @return string
     */
    public function getPreorderText(): string
    {
        return $this->preorderText;
    }

    /**
     * @param string $preorderText
     *
     * @return Position
     */
    public function setPreorderText(string $preorderText): self
    {
        $this->preorderText = $preorderText;
        $this->isPreorderTextDirty = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPreorderTextDirty(): bool
    {
        return $this->isPreorderTextDirty;
    }

    /**
     * @return Stock[]
     */
    public function getStocks(): array
    {
        return $this->stocks;
    }

    /**
     * @param Stock[] $stocks
     *
     * @return Position
     */
    public function setStocks(array $stocks): self
    {
        $this->stocks = $stocks;
        $this->areStocksDirty = true;

        return $this;
    }

    /**
     * @return bool
     */
    public function areStocksDirty(): bool
    {
        return $this->areStocksDirty;
    }

    /**
     * @return Feature[]
     */
    public function getFeatures(): array
    {
        return $this->features;
    }

    /**
     * @param Feature[] $features
     *
     * @return Position
     */
    public function setFeatures(array $features): self
    {
        $this->features = $features;
        $this->areFeaturesDirty = true;

        return $this;
    }

    /**
     * @return bool
     */
    public function areFeaturesDirty(): bool
    {
        return $this->areFeaturesDirty;
    }
}
