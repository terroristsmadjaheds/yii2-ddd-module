<?php

namespace modules\order\entities;

/**
 * Class Stock
 *
 * @package modules\order\entities
 */
class Stock
{
    /**
     * @var string
     */
    private $name = "";

    /**
     * @var int
     */
    private $count = 0;

    /**
     * TODO: Это костыль его необходимо удалить
     *
     * @var int
     */
    private $internalId = 0;

    /**
     * Stock constructor.
     *
     * @param string $name
     * @param int $count
     */
    public function __construct(string $name, int $count)
    {
        $this->setName($name);
        $this->setCount($count);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Stock
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return Stock
     */
    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return int
     */
    public function getInternalId(): int
    {
        return $this->internalId;
    }

    /**
     * @param int $internalId
     *
     * @return Stock
     */
    public function setInternalId(int $internalId): self
    {
        $this->internalId = $internalId;

        return $this;
    }

    /**
     * @return false|string
     */
    public function __toString()
    {
        $result = json_encode(["internalId" => $this->getInternalId(), "name" => $this->getName(), "count" => $this->getCount()]);

        return $result !== false ? $result : "";
    }
}
