<?php

namespace modules\order\exceptions;

/**
 * Class UnableToGetCustomerException
 *
 * @package modules\order\exceptions
 */
class UnableToGetCustomerException extends OrderException
{

}
