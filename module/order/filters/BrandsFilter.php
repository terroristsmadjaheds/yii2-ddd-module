<?php
declare(strict_types=1);

namespace modules\order\filters;

/**
 * Class BrandsFilter
 * @package modules\order\filters
 */
class BrandsFilter
{
    /** @var string */
    private $name = "";

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = trim($name);
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
