<?php

namespace modules\order\interfaces;

use modules\order\entities\Brand;

interface BrandSerializerInterface
{
    /**
     * @param Brand[] $brands
     * @return array
     */
    public function serialize(array $brands): array;

    /**
     * @param Brand $brand
     * @return array
     */
    public function toArray(Brand $brand): array;
}
