<?php

namespace modules\order\interfaces;

/**
 * Interface ProductInfoSource
 *
 * @package modules\order\interfaces
 */
interface PositionServiceConfig
{
    /**
     * @return array
     */
    public function getProductInfoSources(): array;

    /**
     * @param array $productInfoSources
     *
     * @return mixed
     */
    public function setProductInfoSources(array $productInfoSources = []);
}
