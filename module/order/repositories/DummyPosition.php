<?php

namespace modules\order\repositories;

use modules\order\interfaces\PositionRepositoryInterface;

/**
 * Class DummyPosition
 *
 * @package modules\delivery\repository
 *
 * Этот репозиторий просто заглушка. Он имитируем поведение, при котором мы
 * будем получать ничего на запрос позиций
 */
class DummyPosition implements PositionRepositoryInterface
{
    /**
     * @param array $ids
     * @param int $orderId
     *
     * @return array
     */
    public function allPositionsById(array $ids, string $orderId): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return "dummy";
    }

    /**
     * @return bool
     */
    public function isProductInfoSourceEnabled(): bool
    {
        return false;
    }
}
