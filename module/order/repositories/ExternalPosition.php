<?php

namespace modules\order\repositories;

use modules\log\ContextBuilder;
use modules\order\entities\Position;
use modules\order\interfaces\PositionRepositoryInterface;
use modules\order\interfaces\PositionServiceConfig;
use Psr\Log\LoggerInterface;

/**
 * Class ExternalPosition
 *
 * @package modules\order\repositories
 */
abstract class ExternalPosition implements PositionRepositoryInterface
{
    /**
     * @var PositionServiceConfig
     */
    protected $productInfoSource;

    /**
     * @var array
     */
    protected $keyProductInfoSources = [];

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ExternalPosition constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    abstract public function getName(): string;

    /**
     * @param array $ids
     * @param string $orderId
     *
     * @return array
     */
    abstract public function allPositionsById(array $ids, string $orderId): array;

    /**
     * @inheritDoc
     */
    public function expandPositions(array $positions, string $orderID): array
    {
        $toFill = $this->getPositionIdsToFill($positions);
        if (empty($toFill)) {
            $message = sprintf(
                "Источник данных по позициям %s заметил, что заполнять никакую информацию не нужно",
                $this->getName()
            );

            $this->logger->info($message, (new ContextBuilder())->setCategoryCtx(static::class)->build());

            return [];
        }

        $message = sprintf(
            "Из источника данных по позициям %s будем получать инфу о позициях %s",
            $this->getName(),
            implode(", ", $toFill)
        );

        $this->logger->info($message, (new ContextBuilder())->setCategoryCtx(static::class)->build());

        return $this->allPositionsById($toFill, $orderID);
    }

    /**
     * @param Position[] $positions
     *
     * @return string[]
     */
    abstract protected function getPositionIdsToFill(array $positions): array;

    /**
     * @return bool
     */
    public function isProductInfoSourceEnabled(): bool
    {
        $productInfoSources = $this->productInfoSource->getProductInfoSources();

        if (empty($this->keyProductInfoSources) && !empty($productInfoSources)) {
            $this->keyProductInfoSources = array_flip($productInfoSources);
        }

        return array_key_exists($this->getName(), $this->keyProductInfoSources);
    }
}
