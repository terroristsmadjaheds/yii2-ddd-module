<?php

namespace modules\order\repositories;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use modules\core\common\helpers\ArrayHelper;
use modules\delivery\method\constants\LoyaltyCardColor;
use modules\log\ContextBuilder;
use modules\log\OperationContext;
use modules\order\constants\CustomerRole;
use modules\order\entities\Customer;
use modules\order\entities\LoyaltyCard;
use modules\order\exceptions\UnableToGetCustomerException;
use modules\order\interfaces\CustomerRepositoryInterface;
use Psr\Log\LoggerInterface;
use Throwable;

/**
 * Class NpCustomerRepository
 *
 * @package modules\order\repositories
 */
class NpCustomerRepository implements CustomerRepositoryInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $npHost = "";

    /**
     * NpCustomerRepository constructor.
     *
     * @param Client $client
     * @param LoggerInterface $logger
     * @param string $npHost
     */
    public function __construct(Client $client, LoggerInterface $logger, string $npHost)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->npHost = $npHost;
    }

    /**
     * @inheritDoc
     */
    public function getByOrderID(string $orderID): Customer
    {
        $op = new OperationContext("Получение данных о покупателе из репозитория NP");

        try {
            $rawCustomerData = $this->getRawData($orderID);
        } catch (Throwable $e) {
            $op->stop()->failed();

            $ctx = (new ContextBuilder())
                ->setCategoryCtx(static::class)
                ->setOperation($op)
                ->setTopic("Не удалось получить данные о покупателе")
                ->build();

            $this->logger->info("Не удалось получить данные о покупателе из NP", $ctx);

            throw new UnableToGetCustomerException($e->getMessage(), $e->getCode(), $e);
        }

        $op->stop();
        $this->logger->info(
            "",
            (new ContextBuilder())
                ->setCategoryCtx(static::class)
                ->setOperation($op)
                ->build()
        );

        return $this->buildCustomer($rawCustomerData);
    }

    /**
     * Получаем сырые данные о покупателе
     *
     * @param string $orderID
     *
     * @return array
     *
     */
    private function getRawData(string $orderID): array
    {
        $url = sprintf(
            "%s/api/order/info/order?order_id=%s",
            $this->npHost,
            $orderID
        );

        try {
            $response = $this->client->get($url);
        } catch (ClientException $e) {
            $response = $e->getResponse();

            $message = sprintf(
                "Не удалось получить данные о покупателе из репоизтория NP. Получили ответ со статусом %d и телом %s",
                $response ? $response->getStatusCode() : 0,
                $response ? $response->getBody()->getContents() : "\"пустое тело\""
            );

            $this->logger->info(
                $message,
                (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setTopic("Не удалось получить данные о покупателе")
                    ->build()
            );

            throw new UnableToGetCustomerException($e->getMessage(), $e->getCode(), $e);
        }

        $body = $response->getBody()->getContents();
        $message = sprintf(
            "На запрос %s получили успешно ответ %s",
            $url,
            $body
        );

        $this->logger->info($message, (new ContextBuilder())->setCategoryCtx(static::class)->build());

        return json_decode($body);
    }

    /**
     * @param array $rawCustomerData
     *
     * @return Customer
     */
    private function buildCustomer(array $rawCustomerData): Customer
    {
        $loyaltyCardColor = ArrayHelper::getValue($rawCustomerData, "loyalty_card.color", LoyaltyCardColor::WHITE);
        $roles = ArrayHelper::getValue($rawCustomerData, "customer.roles", []);
        $totalSpentAmount = (float) ArrayHelper::getValue($rawCustomerData, "total_spent_amount", 0);
        $isPrivileged = ArrayHelper::getValue($rawCustomerData, "is_privileged", false);

        if ($isPrivileged) {
            $roles[] = CustomerRole::PRIVILEGED;
        }

        $loyaltyCard = new LoyaltyCard($loyaltyCardColor);

        return (new Customer($loyaltyCard))
            ->setTotalSpentAmount($totalSpentAmount)
            ->setExistsRoles($roles);
    }
}
