<?php

namespace modules\order\repositories;

use Enqueue\Consumption\Exception\InvalidArgumentException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Response;
use modules\core\common\constants\HttpStatus;
use modules\currency\CurrencyOperation;
use modules\log\ContextBuilder;
use modules\order\constants\EnumProductInfoConfig;
use modules\order\entities\Brand;
use modules\order\entities\Category;
use modules\order\entities\Position;
use modules\order\entities\Stock;
use modules\order\entities\Tag;
use modules\order\interfaces\PositionRepositoryInterface;
use modules\order\interfaces\PositionServiceConfig;
use Psr\Log\LoggerInterface;

/**
 * Class NpPosition
 *
 * @package modules\delivery\repository
 *
 */
class NpPosition extends ExternalPosition implements PositionRepositoryInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $npHost = "";

    /**
     * NpPosition constructor.
     *
     * @param Client $client
     * @param PositionServiceConfig $productInfoSource
     * @param LoggerInterface $logger
     * @param string $npHost
     */
    public function __construct(Client $client, PositionServiceConfig $productInfoSource, LoggerInterface $logger, string $npHost)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->npHost = $npHost;
        $this->productInfoSource = $productInfoSource;

        parent::__construct($logger);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return EnumProductInfoConfig::NP;
    }

    /**
     * @inheritDoc
     */
    protected function getPositionIdsToFill(array $positions): array
    {
        $toFill = [];

        foreach ($positions as $position) {
            $needToFeelStocks = !$position->areStocksDirty() && empty($position->getStocks());
            $needToFeelBrands = !$position->areBrandsDirty() && empty($position->getBrands());
            $needToFeelCategories = !$position->areCategoriesDirty() && empty($position->getCategories());
            $needToFeelTags = !$position->areTagsDirty() && empty($position->getTags());
            $needToFeelPreorderText = !$position->isPreorderTextDirty() && mb_strlen($position->getPreorderText()) === 0;
            $needToFeelPriceOriginal = !$position->isPriceOriginalDirty() && CurrencyOperation::eq($position->getPriceOriginal(), 0);
            $needToFeelPrice = !$position->isPriceDirty() && CurrencyOperation::eq($position->getPrice(), 0);

            $cond = $needToFeelStocks
                || $needToFeelBrands
                || $needToFeelCategories
                || $needToFeelTags
                || $needToFeelPreorderText
                || $needToFeelPriceOriginal
                || $needToFeelPrice;

            if ($cond) {
                $this->logger->info(sprintf(
                    "По позиции %s недостаточно данных. Будем запрашивать их дополнительно",
                    $position->getId()
                ), (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->build());

                $toFill[] = $position->getId();
            }
        }

        return $toFill;
    }

    /**
     * @param array $ids
     * @param string $orderId
     *
     * @return Position[]
     * @throws ClientException
     */
    public function allPositionsById(array $ids, string $orderId): array
    {
        if (empty($this->npHost)) {
            $this->logger->error(
                sprintf("Не указан хост для обогощения данных"),
                (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setOrderID($orderId)
                    ->build()
            );
            return [];
        }

        $positions = [];
        $rawProductsData = $this->getRawData($ids, $orderId);
        foreach ($ids as $id) {
            if (!array_key_exists($id, $rawProductsData)) {
                $positions[] = (new Position())->setId($id);
                continue;
            }

            $productData = $rawProductsData[$id];
            $position = new Position();
            $position->setId($id);
            $positions[] = $this->fillProduct($productData, $position);
        }

        return $positions;
    }

    /**
     * @param $productData
     * @param Position $position
     *
     * @return Position
     */
    private function fillProduct($productData, Position $position): Position
    {
        try {
            $this->check($productData, ["categories", "brands", "tags", "stocks", "price_original", "price"]);
            $position->setCategories($this->getCategories($productData));
            $position->setTags($this->getTags($productData));
            $position->setBrands($this->getBrands($productData));
            $position->setStocks($this->getStocks($productData));
            $position->setPriceOriginal($productData["price_original"]);
            $position->setPrice($productData["price"]);

            if (array_key_exists("preorder_text", $productData)) {
                $position->setPreorderText($productData["preorder_text"]);
            }

            return $position;
        } catch (\InvalidArgumentException $exception) {
            return (new Position())->setId($position->getId());
        }
    }

    /**
     * Get raw data from NP
     *
     * @param array $positionIds
     * @param string $orderID
     *
     * @return array
     */
    private function getRawData(array $positionIds, string $orderID): array
    {
        if (empty($positionIds)) {
            return [];
        }

        $data = [];
        try {
            /** @var Promise\PromiseInterface[] $promises */
            $promises = [];
            foreach ($positionIds as $id) {
                $promises[$id] = $this->client->getAsync(sprintf(
                    "%s/api/order/info/sku?id=%s&order_id=%s",
                    $this->npHost,
                    $id,
                    $orderID
                ));
            }

            $responses = Promise\settle($promises)->wait();
            foreach ($responses as $id => $resp) {
                if (!isset($resp["value"])) {
                    $this->logBadResponse($resp["reason"], $id, $orderID);
                    continue;
                }

                /** @var Response $resp */
                $resp = $resp["value"];
                $content = $resp->getBody()->getContents();

                $this->logger->info(sprintf(
                    "При попытке получить информацию о товаре %s из заказа" .
                    " %s получили ответ со статусом %d и телом %s",
                    $id,
                    $orderID,
                    $resp->getStatusCode(),
                    $content
                ), (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setOrderID($orderID)
                    ->setTopic("Ошибка при получении информации о товаре")
                    ->build());

                if ($resp->getStatusCode() !== HttpStatus::OK) {
                    continue;
                }

                $data[$id] = json_decode($content, true);
            }
        } catch (Exception $exception) {
            $this->logger->error(
                sprintf(
                    "При попытке получить информацию о товарах из NP" .
                    " словили ошибку %s в файле %s и строке %s\nСтек:\n%s",
                    $exception->getMessage(),
                    $exception->getFile(),
                    $exception->getLine(),
                    $exception->getTraceAsString()
                ),
                (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setOrderID($orderID)
                    ->setTopic("Ошибка при получении информации о товаре")
                    ->build()
            );

            return [];
        }

        return $data;
    }

    /**
     * @param array $response
     *
     * @return Category[]
     */
    private function getCategories(array $response): array
    {
        $response["categories"] = isset($response["categories"]) ? $response["categories"] : [];

        $categories = [];

        foreach ($response["categories"] as $category) {
            $this->check($category, ["id", "name", "slug"]);
            $categories[] = new Category($category["id"], $category["name"], $category["slug"]);
        }

        return $categories;
    }

    /**
     * @param array $response
     *
     * @return Tag[]
     */
    private function getTags(array $response): array
    {
        $response["tags"] = isset($response["tags"]) ? $response["tags"] : [];

        $tags = [];
        foreach ($response["tags"] as $tag) {
            $this->check($tag, ["id", "name", "slug"]);
            $tags[] = new Tag($tag["id"], $tag["name"], $tag["slug"]);
        }

        return $tags;
    }

    /**
     * @param array $response
     *
     * @return array
     */
    private function getBrands(array $response): array
    {
        $response["brands"] = isset($response["brands"]) ? $response["brands"] : [];

        $brands = [];

        foreach ($response["brands"] as $brand) {
            $this->check($brand, ["id", "name", "slug"]);
            $brands[] = new Brand($brand["id"], $brand["name"], $brand["slug"]);
        }

        return $brands;
    }

    /**
     * Возвращает массив складов
     *
     * @param array $response
     *
     * @return array
     */
    private function getStocks(array $response): array
    {
        $response["stocks"] = isset($response["stocks"]) ? $response["stocks"] : [];
        $stocks = [];
        foreach ($response["stocks"] as $stock) {
            $this->check($stock, ["name", "count"]);
            $stocks[] = new Stock($stock["name"], $stock["count"]);
        }
        return $stocks;
    }

    /**
     *
     * Проверяет наличие обязательных ключей в массиве
     *
     * @param array $item
     * @param array $reqKeys
     */
    private function check(array $item, array $reqKeys)
    {
        foreach ($reqKeys as $key) {
            if (!isset($item[$key])) {
                throw new InvalidArgumentException(sprintf("Пропущен обязательный параметр %s", $key));
            }
        }
    }

    /**
     * @param ClientException $resp
     * @param string $skuID
     * @param string $orderID
     */
    private function logBadResponse($resp, string $skuID, string $orderID): void
    {
        $response = $resp->getResponse();

        $this->logger->notice(sprintf(
            "При попытке получить информацию о товаре %s из заказа %s получили ответ со статусом %d и телом %s. " .
            "Текст ошибки: %s. Файл %s строка %s" .
            "Запрос отправили %s на %s с телом %s",
            $skuID,
            $orderID,
            $response ? $response->getStatusCode() : 0,
            $response ? $response->getBody()->getContents() : "пустым",
            $resp->getRequest()->getMethod(),
            $resp->getMessage(),
            $resp->getFile(),
            $resp->getLine(),
            $resp->getRequest()->getUri(),
            $resp->getRequest()->getBody()->getContents()
        ), (new ContextBuilder())
            ->setCategoryCtx(static::class)
            ->setOrderID($orderID)
            ->setTopic("Ошибка при получении информации о товаре")
            ->build());
    }
}
