<?php

namespace modules\order\serializers;

use modules\order\entities\Feature;
use modules\order\interfaces\FeatureSerializer as FeatureSerializerInterface;

/**
 * Class FeatureSerializer
 *
 * @package modules\order\serializers
 */
class FeatureSerializer implements FeatureSerializerInterface
{
    /**
     * @param Feature $feature
     *
     * @return array
     */
    public function toArray(Feature $feature): array
    {
        return [
            "guid" => $feature->getGuid()->toString(),
            "name" => $feature->getName(),
            "value" => $feature->getValue(),
        ];
    }

    /**
     * @param Feature[] $data
     *
     * @return array
     */
    public function serialize(array $data): array
    {
        $result = [];

        foreach ($data as $entity) {
            $result[] = $this->toArray($entity);
        }

        return $result;
    }
}
