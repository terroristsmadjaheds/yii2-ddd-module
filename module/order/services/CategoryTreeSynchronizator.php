<?php

namespace modules\order\services;

use GenericApiSdk\V1\Np\Category\CategoryServiceClientAsync;
use GenericApiSdk\V1\Np\Category\GetFullCategoryTreePromiseInterface;
use GenericApiSdkTransport\Async\PromiseWrapper;
use GenericApiThrift\V1\Exception\InternalServiceException;
use GenericApiThrift\V1\Np\Category\CategoryTreeItemDto;
use modules\db\Connection;
use modules\log\ContextBuilder;
use modules\log\OperationContext;
use modules\mutex\MutexInterface;
use modules\order\repositories\Category;
use Psr\Log\LoggerInterface;
use yii\db\Exception;
use yii\db\Query;

/**
 * Class CategoryTreeSynchronizator
 *
 * @package modules\order\services
 */
class CategoryTreeSynchronizator
{

    /**
     *
     */
    private const KEY_LOCK = "CATEGORY_SYNC";

    /**
     * @var Category
     */
    private $category;

    /**
     * @var CategoryServiceClientAsync
     */
    private $clientAsync;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var MutexInterface
     */
    private $mutex;

    /**
     * CategoryTreeSynchronizator constructor.
     *
     * @param Category $category
     * @param Connection $db
     * @param CategoryServiceClientAsync $clientAsync
     * @param MutexInterface $mutex
     * @param LoggerInterface $logger
     */
    public function __construct(Category $category, Connection $db, CategoryServiceClientAsync $clientAsync, MutexInterface $mutex, LoggerInterface $logger)
    {
        $this->category = $category;
        $this->clientAsync = $clientAsync;
        $this->logger = $logger;
        $this->db = $db;
        $this->mutex = $mutex;
    }

    /**
     * @return string|null
     */
    private function sendResponse(GetFullCategoryTreePromiseInterface $promise): ?string
    {
        try {
            $response = $promise->resolve();
        } catch (InternalServiceException $e) {
            $this->logger->error(
                $e,
                (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setTopic("Ошибка при получении данных о категориях")
                    ->build()
            );
            return null;
        }

        return $this->checkVersion($response);
    }

    /**
     * @param CategoryTreeItemDto $node
     * @param int $parentId
     *
     * @throws Exception
     */
    private function addCategory(CategoryTreeItemDto $node, int $parentId = 0)
    {
        $category = $node->getCategory();

        $this->category->add($category->getId(), $category->getTitle(), $category->getSlug(), $parentId);

        $message = "Добавил {$category->getTitle()} ID({$category->getId()}) родитель $parentId " . PHP_EOL;
        $this->logger->info(
            $message,
            (new ContextBuilder())
                ->setCategoryCtx(static::class)
                ->build()
        );

        foreach ($node->getChildrenList() as $categoryTreeItemDto) {
            $this->addCategory($categoryTreeItemDto, $category->getId());
        }
    }

    /**
     *
     */
    private function clearCategory()
    {
        $this->category->truncate();
    }

    /**
     * @param CategoryTreeItemDto $response
     *
     * @return string
     */
    private function checkVersion(CategoryTreeItemDto $response): string
    {
        $hash = md5(serialize($response));

        $sql = "select hash from category where level=0 and parent_id=0";

        try {
            $hashDb = (new Query())->createCommand($this->db)->setSql($sql)->queryScalar();
        } catch (Exception $e) {
        }

        if ($hash !== $hashDb) {
            return $hash;
        }

        return "";
    }

    /**
     * @param string $hash
     */
    private function setVersion(string $hash)
    {
        try {
            $this->db->createCommand()->update("category", ["hash" => $hash])->execute();
        } catch (Exception $e) {
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function sync(): bool
    {
        if ($this->mutex->lock(self::KEY_LOCK, 30)) {
            $op = new OperationContext("Синхронизация дерева категорий");

            $this->logger->info(
                "Начал синхронизацию",
                (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->build()
            );
            $promiseWrapper = new PromiseWrapper();
            $promise = $this->clientAsync->getFullCategoryTree();
            $promiseWrapper->add($promise);
            $promiseWrapper->waitAll();

            $response = $this->sendResponse($promise);
            if(!is_string($this->sendResponse($promise))) {
                return false;
            }
            if (!empty($hash)) {
                $this->logger->info(
                    "Обновление.",
                    (new ContextBuilder())
                        ->setCategoryCtx(static::class)
                        ->build()
                );
                $transaction = $this->db->beginTransaction();
                try {
                    $this->clearCategory();
                    $this->addCategory($response);
                    $this->setVersion($hash);
                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $op->failed();
                    $this->logger->error(
                        $e,
                        (new ContextBuilder())
                            ->setCategoryCtx(static::class)
                            ->setTopic("Ошибка при сохранении дерева категорий")
                            ->build()
                    );
                }
            } else {
                $this->logger->info(
                    "Нет изменений",
                    (new ContextBuilder())
                        ->setCategoryCtx(static::class)
                        ->build()
                );
            }

            $op->stop();

            $this->logger->info(
                sprintf("Синхронизация завершена. Результат: %s . Продолжительность: %f", $op->getResult(), $op->getTime()),
                (new ContextBuilder())
                    ->setOperation($op)
                    ->setCategoryCtx(static::class)
                    ->build()
            );
        }

        return true;
    }
}
