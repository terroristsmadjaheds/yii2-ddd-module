<?php

namespace modules\order\services;

use modules\log\ContextBuilder;
use modules\log\OperationContext;
use modules\order\entities\Position;
use modules\order\interfaces\PositionRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class PositionService
 *
 * @package modules\order\services
 */
class PositionService
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PositionRepositoryInterface[]
     */
    private $positionsRepository;

    /**
     * PositionService constructor.
     *
     * @param PositionRepositoryInterface[] $positionsRepository
     * @param LoggerInterface $logger
     */
    public function __construct(array $positionsRepository, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->positionsRepository = $positionsRepository;
    }

    /**
     * @param array $ids
     * @param int $orderId
     *
     * @return Position[]
     */
    public function allPositionsById(array $ids, int $orderId): array
    {
        $opCommonRep = new OperationContext("Получение данных из репозиториев");

        $result = [];

        foreach ($this->positionsRepository as $positionRepository) {
            $op = new OperationContext("Получение данных о позициях из " . $positionRepository->getName());

            if (!$positionRepository->isProductInfoSourceEnabled()) {
                $this->logger->info($positionRepository->getName() . " выключен", (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setTopic("Получение данных из репозиториев")
                    ->build());
                continue;
            }

            $result = array_merge($result, $positionRepository->allPositionsById($ids, $orderId));

            $op->stop();
        }

        $opCommonRep->stop();

        return $result;
    }

    /**
     * @param array $positions
     * @param string $orderID
     *
     * @return array
     */
    public function expandPositions(array $positions, string $orderID): array
    {
        $opCommonRep = new OperationContext("Получение данных из репозиториев");

        $result = [];

        foreach ($this->positionsRepository as $positionRepository) {
            $op = new OperationContext("Получение данных о позициях из " . $positionRepository->getName());
            if (!$positionRepository->isProductInfoSourceEnabled()) {
                $this->logger->info($positionRepository->getName() . " выключен", (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setTopic("Получение данных из репозиториев")
                    ->build());
                continue;
            }
            $repoData = $positionRepository->expandPositions($positions, $orderID);
            $op->stop();
            $result = array_merge($result, $repoData);
        }

        $opCommonRep->stop();
        $this->logAdditionalData();
        $this->logger->info("Получил данные из внешних репозиториев", (new ContextBuilder())
            ->setCategoryCtx(static::class)
            ->setOperation($opCommonRep)
            ->setTopic("Получение данных из репозиториев")
            ->build());

        return $result;
    }

    /**
     * @param $positionRepository
     * @param $positions
     * @param $op
     */
    private function logAdditionalData($positionRepository, $positions, $op): void {
        $message = sprintf(
            "Получил данные из %s количество запросов %d",
            $positionRepository->getName(),
            count($positions)
        );

        if (empty($repoData)) {
            $message = sprintf(
                "Источник данных %s вернул пустой набор позиций",
                $positionRepository->getName()
            );
        }

        $this->logger->info($message, (new ContextBuilder())
            ->setCategoryCtx(static::class)
            ->setOperation($op)
            ->setTopic("Получение данных из репозиториев")
            ->build());
    }

}
