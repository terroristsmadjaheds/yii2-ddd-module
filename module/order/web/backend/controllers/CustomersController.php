<?php
declare(strict_types=1);

namespace modules\order\web\backend\controllers;

use modules\net\http\AdminController;
use modules\order\web\backend\controllers\customers\CustomersAction;

/**
 * Class CustomersController
 * @package modules\order\web\backend\controllers
 */
class CustomersController extends AdminController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            "roles" => [
                "class" => CustomersAction::class
            ]
        ];
    }
}
