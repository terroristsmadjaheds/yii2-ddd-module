<?php

namespace modules\order\web\backend\controllers;

use modules\net\http\AdminController;
use modules\order\web\backend\controllers\position\BrandsAction;
use modules\order\web\backend\controllers\position\CategoryAction;
use modules\order\web\backend\controllers\position\FeaturesAction;
use yii\base\InvalidConfigException;

/**
 * Class PositionController
 *
 * @package modules\order\web\backend\controllers
 */
class PositionController extends AdminController
{
    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function actions()
    {
        return [
            "features" => [
                "class" => FeaturesAction::class,
            ],
            "brands" => [
                "class" => BrandsAction::class,
                "name" => (string) request()->getQueryParam("name", "")
            ],
            "categories" => [
                "class" => CategoryAction::class,
            ],
        ];
    }
}
