<?php

namespace modules\order\web\backend\controllers\position;

use modules\order\repositories\Category;
use modules\order\serializers\CategorySerializer;
use yii\base\Action;
use yii\db\Exception as DbException;

/**
 * Class CategoryAction
 *
 * @package modules\order\backend\controllers\position
 */
class CategoryAction extends Action
{
    /**
     * @var Category
     */
    private $category;

    /**
     * @var CategorySerializer
     */
    private $categorySerializer;

    /**
     * CategoryAction constructor.
     *
     * @param $id
     * @param $controller
     * @param Category $category
     * @param CategorySerializer $categorySerializer
     * @param array $config
     */
    public function __construct($id, $controller, Category $category, CategorySerializer $categorySerializer, $config = [])
    {
        parent::__construct($id, $controller, $config);
        $this->category = $category;
        $this->categorySerializer = $categorySerializer;
    }

    /**
     * @param string $title
     *
     * @return \modules\order\entities\category\Category[]
     * @throws DbException
     */
    public function run(string $title = "")
    {
        return $this->categorySerializer->serialize($this->category->all($title));
    }
}
