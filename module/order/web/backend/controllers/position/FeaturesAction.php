<?php

namespace modules\order\web\backend\controllers\position;

use modules\order\exceptions\UnableToGetFeatureException;
use modules\order\interfaces\FeatureSerializer;
use modules\order\interfaces\FeatureService;
use yii\base\Action;

/**
 * Class FeaturesAction
 *
 * @package modules\order\position\web\backend\controllers\position
 */
class FeaturesAction extends Action
{
    /**
     * @var FeatureService
     */
    private $featureService;

    /**
     * @var FeatureSerializer
     */
    private $featureSerializer;

    /**
     * FeaturesAction constructor.
     *
     * @param $id
     * @param $controller
     * @param FeatureService $featureService
     * @param FeatureSerializer $featureSerializer
     * @param array $config
     */
    public function __construct($id, $controller, FeatureService $featureService, FeatureSerializer $featureSerializer, $config = [])
    {
        parent::__construct($id, $controller, $config);
        $this->featureService = $featureService;
        $this->featureSerializer = $featureSerializer;
    }

    /**
     * @return array
     */
    public function run(): array
    {
        $result = [];

        try {
            $result = $this->featureService->getFeatures();
        } catch (UnableToGetFeatureException $exception) {
            return $result;
        }

        return $this->featureSerializer->serialize($result);
    }
}
