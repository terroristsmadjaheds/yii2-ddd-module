<?php

namespace modules\partner\exceptions;

/**
 * Class DeleteOperationException
 *
 * @package modules\partner\exceptions
 */
class DeleteOperationException extends PartnerException
{
    public const ERROR_CODE_DB = 1;
}
