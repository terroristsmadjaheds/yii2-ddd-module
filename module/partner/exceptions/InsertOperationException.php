<?php

namespace modules\partner\exceptions;

/**
 * Class InsertOperationException
 *
 * @package modules\partner\exceptions
 */
class InsertOperationException extends PartnerException
{
    public const ERROR_CODE_DB = 1;
    public const ERROR_DUPLICATE_ENTRY = 2;
}
