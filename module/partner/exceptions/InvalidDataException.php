<?php

namespace modules\partner\exceptions;

/**
 * Class InvalidDataException
 *
 * @package modules\partner\exceptions
 */
class InvalidDataException extends PartnerException
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     *
     * @return InvalidDataException
     */
    public function setErrors(array $errors): InvalidDataException
    {
        $this->errors = $errors;
        return $this;
    }
}
