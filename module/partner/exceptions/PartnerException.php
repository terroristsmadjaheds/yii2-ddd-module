<?php

namespace modules\partner\exceptions;

use LogicException;

/**
 * Class PartnerNotFound
 *
 * @package modules\partner\exceptions
 */
class PartnerException extends LogicException
{
}
