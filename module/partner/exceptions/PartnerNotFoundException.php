<?php

namespace modules\partner\exceptions;

/**
 * Class PartnerNotFound
 *
 * @package modules\partner\exceptions
 */
class PartnerNotFoundException extends PartnerException
{
    public const ERROR_CODE_FIND = 1;

    public const ERROR_CODE_ALL = 2;

    public const ERROR_CODE_DELETE = 3;

    public const ERROR_CODE_UPDATE = 4;
}
