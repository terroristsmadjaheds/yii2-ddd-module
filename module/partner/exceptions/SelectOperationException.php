<?php

namespace modules\partner\exceptions;

/**
 * Class UpdateOperationException
 *
 * @package modules\partner\exceptions
 */
class SelectOperationException extends PartnerException
{
    public const ERROR_CODE_DB = 1;

    public const ERROR_CODE = 2;
}
