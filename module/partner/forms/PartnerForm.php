<?php

namespace modules\partner\forms;

use yii\base\Model;

/**
 * Class PartnerForm
 *
 * @package modules\partner\forms
 */
class PartnerForm extends Model
{
    /**
     * @var string
     */
    public $id = "";

    /**
     * @var string
     */
    public $name = "";

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ["id", "name"], "required",
            ],
            [
                ["id", "name"], "string", "max" => 50,
            ],
        ];
    }
}
