<?php

namespace modules\partner\serializers;

use modules\partner\entities\Partner;

/**
 * Class PartnerSerializer
 *
 * @package modules\partner\serializers
 */
class PartnerSerializer
{
    /**
     * @param Partner $partner
     *
     * @return array
     */
    public function serialize(Partner $partner): array
    {
        return [
            "id" => $partner->getId(),
            "name" => $partner->getName(),
        ];
    }

    /**
     * @param array $items
     *
     * @return array
     */
    public function serializeArray(array $items): array
    {
        $res = [];

        foreach ($items as $item) {
            $res[] = $this->serialize($item);
        }

        return $res;
    }
}
