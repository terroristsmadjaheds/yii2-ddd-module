<?php

namespace modules\partner\validators;

use modules\partner\exceptions\SelectOperationException;
use modules\partner\filters\PartnerFilter;
use modules\partner\repositories\PartnerRepository;
use yii\validators\Validator;

/**
 * Class PartnerExistsValidator
 */
class PartnerExistsValidator extends Validator
{
    /**
     * @var PartnerRepository
     */
    private $partnerRepository;

    /**
     * @var bool
     */
    public $allowEmpty = false;

    /**
     * @var string $message
     */
    public $message = "Партнер {value} не найден";

    /**
     * PickUpExistsValidator constructor.
     *
     * @param PartnerRepository $partnerRepository
     * @param array $config
     */
    public function __construct(PartnerRepository $partnerRepository, $config = [])
    {
        parent::__construct($config);
        $this->partnerRepository = $partnerRepository;
    }

    /**
     * @param mixed $value
     *
     * @return array|null
     */
    protected function validateValue($value)
    {
        try {
            if ((!$value && $this->allowEmpty) || $this->partnerRepository->one((new PartnerFilter())->setIds([$value]))) {
                return null;
            }
        } catch (SelectOperationException $e) {
            $this->message = $e->getMessage() . ". " . $e->getPrevious()->getMessage();
        }

        return [
            $this->message,
            [
                "value" => $value,
            ],
        ];
    }
}
