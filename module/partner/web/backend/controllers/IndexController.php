<?php

namespace modules\partner\web\backend\controllers;

use modules\core\common\helpers\ArrayHelper;
use modules\net\http\AdminController;
use modules\partner\web\backend\controllers\index\CreateAction;
use modules\partner\web\backend\controllers\index\DeleteAction;
use modules\partner\web\backend\controllers\index\ListAction;
use modules\partner\web\backend\controllers\index\UpdateAction;
use modules\partner\web\backend\controllers\index\ViewAction;
use yii\base\InvalidConfigException;
use yii\web\Request;

/**
 * Class IndexController
 *
 * @package modules\partner\web\backend\controllers
 */
class IndexController extends AdminController
{
    /**
     * @var Request
     */
    private $req;

    /**
     * IndexController constructor.
     *
     * @param $id
     * @param $module
     * @param Request $req
     * @param array $config
     */
    public function __construct($id, $module, Request $req, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->req = $req;
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            "create" => [
                "class" => CreateAction::class,
                "params" => $this->req->getBodyParams(),
            ],
            "update" => [
                "class" => UpdateAction::class,
                "params" => $this->req->getBodyParams(),
            ],
            "delete" => DeleteAction::class,
            "view" => ViewAction::class,
            "index" => [
                "class" => ListAction::class,
                "page" => ((int) $this->req->getQueryParam("page", 1)) - 1,
                "perPage" => (int) $this->req->getQueryParam("per-page", 20),
                "sort" => (string) $this->req->getQueryParam("sort", ""),
            ],
        ]);
    }
}
