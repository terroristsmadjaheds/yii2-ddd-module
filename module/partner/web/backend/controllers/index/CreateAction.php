<?php

namespace modules\partner\web\backend\controllers\index;

use modules\core\common\components\web\Response;
use modules\core\common\constants\HttpStatus;
use modules\core\common\exceptions\http\InternalServerErrorException;
use modules\core\common\exceptions\http\UnprocessableEntityException;
use modules\partner\exceptions\InvalidDataException;
use modules\partner\forms\PartnerForm;
use modules\partner\serializers\PartnerSerializer;
use modules\partner\services\Partner;
use Psr\Log\LoggerInterface;
use yii\base\Action;
use yii\db\Exception;

/**
 * Class CreateAction
 *
 * @package modules\partner\web\controllers
 */
class CreateAction extends Action
{
    /**
     * @var array
     */
    public $params = [];

    /**
     * @var Partner
     */
    private $partnerService;

    /**
     * @var PartnerSerializer
     */
    private $partnerSerializer;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ListAction constructor.
     *
     * @param $id
     * @param $controller
     * @param Partner $partnerService
     * @param PartnerSerializer $partnerSerializer
     * @param Response $response
     * @param LoggerInterface $logger
     * @param array $config
     */
    public function __construct($id, $controller, Partner $partnerService, PartnerSerializer $partnerSerializer, Response $response, LoggerInterface $logger, $config = [])
    {
        parent::__construct($id, $controller, $config);
        $this->partnerService = $partnerService;
        $this->partnerSerializer = $partnerSerializer;
        $this->response = $response;
        $this->logger = $logger;
    }

    /**
     * @return array
     * @throws UnprocessableEntityException
     */
    public function run()
    {
        $partnerForm = new PartnerForm();
        $partnerForm->load($this->params, "");
        try {
            $partner = $this->partnerService->insert($partnerForm);
        } catch (InvalidDataException $exception) {
            throw new UnprocessableEntityException(
                "Не удалось сохранить партнера",
                HttpStatus::UNPROCESSABLE_ENTITY,
                $exception->getErrors()
            );
        }

        return $this->partnerSerializer->serialize($partner);
    }
}
