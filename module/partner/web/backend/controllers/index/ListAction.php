<?php

namespace modules\partner\web\backend\controllers\index;

use modules\partner\exceptions\PartnerNotFoundException;
use modules\partner\exceptions\SelectOperationException;
use modules\partner\filters\PartnerFilter;
use modules\partner\serializers\PartnerSerializer;
use modules\partner\services\Partner;
use yii\base\Action;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;

/**
 * Class ListAction
 *
 * @package modules\partner\web\backend\controllers\index
 */
class ListAction extends Action
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var int
     */
    public $perPage = 20;

    /**
     * @var string
     */
    public $sort = "";

    /**
     * @var Partner
     */
    private $partnerService;

    /**
     * @var PartnerSerializer
     */
    private $partnerSerializer;

    /**
     * ListAction constructor.
     *
     * @param $id
     * @param $controller
     * @param Partner $partnerService
     * @param PartnerSerializer $partnerSerializer
     * @param array $config
     */
    public function __construct($id, $controller, Partner $partnerService, PartnerSerializer $partnerSerializer, $config = [])
    {
        parent::__construct($id, $controller, $config);
        $this->partnerService = $partnerService;
        $this->partnerSerializer = $partnerSerializer;
    }

    /**
     * @return array|ArrayDataProvider
     */
    public function run()
    {
        try {
            $models = $this->partnerService->getAll((new PartnerFilter()));
        } catch (SelectOperationException $e) {
            $previous = $e->getPrevious();
            if ($previous instanceof PartnerNotFoundException && $previous->getCode() == PartnerNotFoundException::ERROR_CODE_ALL) {
                return [];
            }

            throw $e;
        }

        return new ArrayDataProvider([
            "allModels" => $this->partnerSerializer->serializeArray($models),
            "pagination" => new Pagination([
                "totalCount" => count($models),
                "page" => $this->page,
                "pageSize" => $this->perPage,
            ]),
        ]);
    }
}
